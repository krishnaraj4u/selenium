/**
 * 
 */
package com.pramaniks.selenium.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.io.IOUtils;
//import org.json.JSONObject;

import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

/**
 * @author Shweta
 *
 */
public class CommonUtil {
	
	public static JsonObject getJsonObject(final String fileName) throws Exception {
		JsonObject jsonObject = null;
		InputStream inputStream = null;
		JsonReader reader = null;
		try {
			final URL resource = CommonUtil.class.getResource("/data/"+fileName);				
			final File file = new File(resource.toURI());
			inputStream = new FileInputStream(file);
			final String jsonData = IOUtils.toString(inputStream);			
			System.out.println(jsonData);
			reader = Json.createReader(new StringReader(jsonData));
			jsonObject = reader.readObject();			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			IOUtils.closeQuietly(inputStream);
			reader.close();
		}
		return jsonObject;
		
	}
}
