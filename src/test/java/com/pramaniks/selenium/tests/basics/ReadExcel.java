package com.pramaniks.selenium.tests.basics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ReadExcel {
	
	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		
		FileInputStream fin=new FileInputStream("F:\\Logininfo.xlsx");
		Workbook wb=WorkbookFactory.create(fin);
		//to count no. of sheets
		int sheetcount=wb.getNumberOfSheets();
		System.out.println("No.of sheets:"+sheetcount);
		//to print name of sheets
		for(int i=0;i<sheetcount;i++)
		{
			System.out.println(wb.getSheetName(i));
		}
		
	}

}
