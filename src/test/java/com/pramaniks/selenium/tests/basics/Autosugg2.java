package com.pramaniks.selenium.tests.basics;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Autosugg2 {

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver=new FirefoxDriver();
		driver.get("http://www.google.com");
		driver.findElement(By.id("lst-ib")).sendKeys("katrina");
		Thread.sleep(3000);
        //storing the info from auto suggestion
		List<WebElement> allinfo=driver.findElements(By.xpath("//div[@class='sbsb_a']/ul/li"));
		System.out.println("No. of informations: "+allinfo.size());
		allinfo.get(2).click();

	}
}
