package com.pramaniks.selenium.tests.basics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class WriteintoCell {

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		
		FileInputStream fin=new FileInputStream("F:\\Logininfo.xlsx");
		Workbook wb=WorkbookFactory.create(fin);
		Sheet s=wb.getSheet("unix");
		s.getRow(0).getCell(0).setCellValue("sql");
		
		FileOutputStream fout=new FileOutputStream("F:\\Logininfo.xlsx");
		wb.write(fout);
	    fout.flush();
	    wb.close();
	    fout.close();
		
	}
}
