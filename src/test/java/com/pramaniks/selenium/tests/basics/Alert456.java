package com.pramaniks.selenium.tests.basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Alert456 {

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver=new FirefoxDriver();
		driver.get("http://ksrtc.in/");
		driver.findElement(By.id("matchStartPlace")).sendKeys("Bangalore");
		driver.findElement(By.linkText("Search Services")).click();
		System.out.println(driver.switchTo().alert().getText());
		Thread.sleep(4000);
		driver.switchTo().alert().accept();


	}

}
