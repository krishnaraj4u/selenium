package com.pramaniks.selenium.tests.basics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilterInputStream;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ReadExcel2 {
	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		
		FileInputStream fin=new FileInputStream("F:\\Logininfo.xlsx");
		Workbook wb=WorkbookFactory.create(fin);
		Sheet s=wb.getSheet("unix");
		int rowCount=s.getLastRowNum();
		System.out.println("No. of active rows "+(rowCount+1));
		Row r1=s.getRow(0);
		int cellCount=r1.getLastCellNum();
		System.out.println("No. of cells present in 1st row "+cellCount);
		wb.close();
		fin.close();
		
	}

}
