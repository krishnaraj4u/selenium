package com.pramaniks.selenium.tests.basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Navigation {


	public static void main(String[] args) throws InterruptedException {

		WebDriver driver=new FirefoxDriver();
		driver.get("http://127.0.0.1/login.do");
		driver.findElement(By.name("username")).sendKeys("admin");
		driver.findElement(By.name("pwd")).sendKeys("manager");
		driver.findElement(By.linkText("Login")).click();
		driver.findElement(By.linkText("Settings")).click();
		driver.findElement(By.xpath("//div[text()='Users']")).click();
		driver.navigate().back();
		Thread.sleep(2000);
		driver.navigate().forward();
		driver.close();

	}

}
