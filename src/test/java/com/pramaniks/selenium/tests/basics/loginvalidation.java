package com.pramaniks.selenium.tests.basics;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
public class loginvalidation {
	
	public static void main(String[] args) throws InterruptedException {

		negativetesting();
		positivetesting();
		
	}
	 static void negativetesting()
	    {
	    	WebDriver driver=new FirefoxDriver();
			driver.get("http://127.0.0.1/login.do");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    driver.findElement(By.name("username")).sendKeys("abcd");
					driver.findElement(By.name("pwd")).sendKeys("efgh");
					driver.findElement(By.linkText("Login")).click();
					if(driver.getTitle().equalsIgnoreCase("actiTIME - Enter Time-Track"))
					{
						System.out.println("login successful");
					}
					
					else
					{
						System.out.println("login failed");
					} 
					driver.close();
	    }
    static void positivetesting()
	
	{
		
    	WebDriver driver=new FirefoxDriver();
		driver.get("http://127.0.0.1/login.do");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    driver.findElement(By.name("username")).sendKeys("admin");
				driver.findElement(By.name("pwd")).sendKeys("manager");
				driver.findElement(By.linkText("Login")).click();
				if(driver.getTitle().equalsIgnoreCase("actiTIME - Enter Time-Track"))
				{
					System.out.println("login successful");
				}
				
				else
				{
					System.out.println("login failed");
				} 
				driver.close();
}
   
}