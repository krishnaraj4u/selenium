package com.pramaniks.selenium.tests.basics;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Hyperlinktext2 {

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver=new FirefoxDriver();
		driver.get("http://127.0.0.1/login.do");
		driver.findElement(By.name("username")).sendKeys("admin");
		driver.findElement(By.name("pwd")).sendKeys("manager");
		driver.findElement(By.linkText("Login")).click();
		Thread.sleep(8000);
		List<WebElement> links=driver.findElements(By.tagName("a"));
		System.out.println("no. of links " +links.size());
		for(WebElement w:links)
		{
			System.out.println(w.getText());
		}
        driver.close();
	}

}
