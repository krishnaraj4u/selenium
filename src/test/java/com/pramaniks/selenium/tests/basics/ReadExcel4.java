package com.pramaniks.selenium.tests.basics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ReadExcel4 {
	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		
		FileInputStream fin=new FileInputStream("F:\\Logininfo.xlsx");
		Workbook wb=WorkbookFactory.create(fin);
		Sheet s=wb.getSheet("sql");
		Row r1=s.getRow(1);
		Cell c=r1.getCell(2);
		Double val=c.getNumericCellValue();
		System.out.println(val);
		
	}

}
