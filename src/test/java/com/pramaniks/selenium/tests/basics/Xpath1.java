package com.pramaniks.selenium.tests.basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Xpath1 {

	public static void main(String[] args) {

		WebDriver driver=new FirefoxDriver();
		driver.get("file:///F:/xpath1.html");
        driver.findElement(By.xpath("/html/body/input[1]")).sendKeys("selenium");
        driver.findElement(By.xpath("/html/body/input[2]")).sendKeys("java");
        driver.findElement(By.xpath("/html/body/a[2]")).click();

	}

}
