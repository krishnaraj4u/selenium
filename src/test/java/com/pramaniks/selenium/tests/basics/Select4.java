package com.pramaniks.selenium.tests.basics;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Select4 {

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver=new FirefoxDriver();
		driver.get("https://www.facebook.com/login/");
		Thread.sleep(8000);
		driver.findElement(By.linkText("Sign Up")).click();
		Thread.sleep(3000);
		WebElement ad=driver.findElement(By.name("birthday_day"));
		Select s2=new Select(ad);
		int info=s2.getOptions().size();
		//select all the options
		for(int i=0;i<info;i++)
		{
			s2.selectByIndex(i);
		}
		//s2.deselectAll();
	}


}
