package com.pramaniks.selenium.tests.basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Select3 {

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver=new FirefoxDriver();
		driver.get("https://www.facebook.com/login/");
		driver.findElement(By.linkText("Sign Up")).click();
		Thread.sleep(3000);
		WebElement ad=driver.findElement(By.name("birthday_day"));
		Select s1=new Select(ad);
		s1.selectByIndex(4);
		//s1.selectByValue("2");
		//s1.selectByVisibleText("7");
	}

}
