package com.pramaniks.selenium.tests.basics;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Select5 {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver=new FirefoxDriver();
		driver.get("https://www.facebook.com/login/");
		Thread.sleep(8000);
		driver.findElement(By.linkText("Sign Up")).click();
		Thread.sleep(3000);
		WebElement add=driver.findElement(By.name("birthday_day"));
		Select s3=new Select(add);
		List<WebElement> allinfo=s3.getOptions();
		for(WebElement w1:allinfo)
		{
			System.out.println(w1.getText());
		}

	}

}
