package com.pramaniks.selenium.tests.basics;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AutoSugg {


	public static void main(String[] args) throws InterruptedException {

		WebDriver driver=new FirefoxDriver();
		driver.get("http://www.google.com");
		driver.findElement(By.id("lst-ib")).sendKeys("Katrina");
		Thread.sleep(3000);
        //storing the info from auto suggestion
		List<WebElement> allinfo=driver.findElements(By.xpath("//div[@class='sbsb_a']/ul/li"));
		System.out.println("No. of informations: "+allinfo.size());

		//printing the informations

		for(WebElement w:allinfo)
		{
			System.out.println(w.getText());
		}

	}


}
