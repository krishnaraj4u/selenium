package com.pramaniks.selenium.tests.basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class MultipleSelect456 {


	public static void main(String[] args) {

		WebDriver driver=new FirefoxDriver();
		driver.get("file:///F:/shwetatext8.html");
		WebElement addr1=driver.findElement(By.name("company"));
		Select s2=new Select(addr1);
		s2.selectByIndex(1);
		s2.selectByValue("126");
		s2.selectByVisibleText("Test Yantra");

		s2.deselectByIndex(3);
		s2.deselectByValue("127");
		s2.deselectByVisibleText("Google");

		s2.selectByIndex(1);
		s2.selectByValue("126");
		s2.selectByVisibleText("Test Yantra");

		s2.deselectAll();


	}

}
