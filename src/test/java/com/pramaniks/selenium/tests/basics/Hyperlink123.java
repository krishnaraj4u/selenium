package com.pramaniks.selenium.tests.basics;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Hyperlink123 {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver=new FirefoxDriver();
		driver.get("http://www.google.com");

		driver.findElement(By.id("lst-ib")).sendKeys("ktm");
		driver.findElement(By.name("btnG")).click();
		Thread.sleep(7000);
		List<WebElement> hyperaddr=driver.findElements(By.tagName("a"));
		System.out.println("No. of Hyperlinks " +hyperaddr.size());
		driver.close();
	}

}
