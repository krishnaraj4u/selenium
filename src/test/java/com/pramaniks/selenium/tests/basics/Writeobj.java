package com.pramaniks.selenium.tests.basics;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class A implements Serializable {
	double cc;
	String name;
	double price;
	A(double cc,String name,double price)
	{
		this.cc=cc;
		this.name=name;
		this.price=price;
	}
	void carinfo()
	{
		System.out.println("-----car details--------");
		System.out.println("car name:"+name);
		System.out.println("power:"+cc);
		System.out.println("price:"+price);
		
	}

}

public class Writeobj
{
	public static void main(String[] args) throws IOException {
		FileOutputStream fout=new FileOutputStream("F:\\info.ser");
		ObjectOutputStream ob=new ObjectOutputStream(fout);
		A a1=new A(2.2,"maruti",1400000.0);
		ob.writeObject(a1);
		ob.flush();
		ob.close();
		fout.close();
		
	}
}