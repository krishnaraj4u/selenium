package com.pramaniks.selenium.tests.basics;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Window2 {

	public static void main(String[] args) {
		
		WebDriver driver=new FirefoxDriver();
		driver.get("http://127.0.0.1/login.do");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		System.out.println(driver.getTitle());
		driver.findElement(By.linkText("Actimind Inc.")).click();
	    
	    
		Iterator<String> wh=driver.getWindowHandles().iterator();
		/*while(wh.hasNext())
		{
			System.out.println(wh.next());
		}*/
		String parent=wh.next();
		String child=wh.next();
		driver.switchTo().window(child);
		System.out.println(driver.getTitle());
		driver.close();
		driver.switchTo().window(parent);
		System.out.println("Back to previous window " +driver.getTitle());
	}
}
