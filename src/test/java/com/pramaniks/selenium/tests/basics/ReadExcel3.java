package com.pramaniks.selenium.tests.basics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ReadExcel3 {
	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		
		FileInputStream fin=new FileInputStream("F:\\Logininfo.xlsx");
		Workbook wb=WorkbookFactory.create(fin);
		Sheet s=wb.getSheet("unix");
		Row r1=s.getRow(0);
		Cell c=r1.getCell(0);
		String val=c.getStringCellValue();
		System.out.println("value of 1st cell "+val);
		
	}

}
