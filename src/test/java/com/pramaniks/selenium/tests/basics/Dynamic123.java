package com.pramaniks.selenium.tests.basics;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Dynamic123 {
	
	public static void main(String[] args) throws InterruptedException {

		WebDriver driver=new FirefoxDriver();
		driver.get("http://127.0.0.1/login.do");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.name("username")).sendKeys("admin");
		driver.findElement(By.name("pwd")).sendKeys("manager");
		driver.findElement(By.linkText("Login")).click();
		driver.findElement(By.linkText("Settings")).click();
		driver.findElement(By.linkText("Billing Types")).click();
		//driver.findElement(By.xpath("//tr[td/a[text()='Selenium']]//td[6]/a")).click();
		driver.findElement(By.xpath("//a[text()='Selenium']/../..//a[contains(text(),'delete')]")).click();
		Thread.sleep(3000);
        driver.switchTo().alert().accept();
		
		
		
}
}
