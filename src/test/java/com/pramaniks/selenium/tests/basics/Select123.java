package com.pramaniks.selenium.tests.basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Select123 {


	public static void main(String[] args) {

       WebDriver driver=new FirefoxDriver();
       driver.get("http://127.0.0.1/login.do");
       driver.findElement(By.name("username")).sendKeys("admin");
       driver.findElement(By.name("pwd")).sendKeys("manager");
       driver.findElement(By.linkText("Login")).click();
       driver.findElement(By.xpath("//div[text()='Tasks']")).click();
       driver.findElement(By.partialLinkText("Projects")).click();
       WebElement addr=driver.findElement(By.name("selectedCustomer"));
       Select s1=new Select(addr);
       //s1.selectByIndex(1);
       s1.selectByValue("5");
       //s1.selectByVisibleText("M&Z Partnership");
	}

}
