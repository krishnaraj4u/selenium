package com.pramaniks.selenium.tests.basics;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Calenderpopup {

	public static void main(String[] args) {
		
		WebDriver driver=new FirefoxDriver();
		driver.get("http://127.0.0.1/login.do");;
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.name("username")).sendKeys("admin");
		driver.findElement(By.name("pwd")).sendKeys("manager");
		driver.findElement(By.linkText("Login")).click();
		driver.findElement(By.linkText("Tasks")).click();
		driver.findElement(By.linkText("Completed Tasks")).click();
		driver.findElement(By.id("completionDateFrom_userText")).sendKeys("Nov 20,2015");
		driver.findElement(By.id("completionDateTo_userText")).sendKeys("Dec 20,2015");
		driver.findElement(By.id("tasksFilterSubmitButton")).click();
	}
}
