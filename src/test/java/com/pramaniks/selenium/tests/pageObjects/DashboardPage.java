/**
 * 
 */
package com.pramaniks.selenium.tests.pageObjects;

import javax.json.JsonObject;

import org.openqa.selenium.WebDriver;

/**
 * @author Shweta
 *
 */
public class DashboardPage {
	private WebDriver driver;
	private JsonObject jsonObject;
	/**
	 * @param driver
	 * @param jsonObject
	 */
	public DashboardPage(WebDriver driver, JsonObject jsonObject) {
		super();
		this.driver = driver;
		this.jsonObject = jsonObject;
	}
	
	public void testDashboardPage() throws Exception {
		getDriver().getTitle().equals("actiTIME - Enter Time-Track");		
	}
	
	/**
	 * @return the driver
	 */
	public WebDriver getDriver() {
		return driver;
	}
	/**
	 * @param driver the driver to set
	 */
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	/**
	 * @return the jsonObject
	 */
	public JsonObject getJsonObject() {
		return jsonObject;
	}
	/**
	 * @param jsonObject the jsonObject to set
	 */
	public void setJsonObject(JsonObject jsonObject) {
		this.jsonObject = jsonObject;
	}
	
	
}
