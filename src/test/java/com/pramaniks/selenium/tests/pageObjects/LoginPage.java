/**
 * 
 */
package com.pramaniks.selenium.tests.pageObjects;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Shweta
 *
 */
public class LoginPage {
	private WebDriver driver;
	private JsonObject jsonObject;
	
	public LoginPage(final WebDriver webDriver, final JsonObject jsonObject) {
		setDriver(webDriver);
		setJsonObject(jsonObject);
	}
	
	public void testLoginPage() throws Exception {
		testNegative();
		testSecurity();
		testPositive();
	}
	
	private void testNegative() throws Exception{
		JsonArray invalidLoginIdsArray = jsonObject.getJsonArray("invalidLoginIds");
		for (JsonValue jsonValue : invalidLoginIdsArray) {	
			JsonObject jsonObject = (JsonObject)jsonValue;
			clearLoginForm();
			checkEmptyValue();
			getDriver().findElement(By.name("username")).sendKeys(jsonObject.getString("username"));
			getDriver().findElement(By.name("pwd")).sendKeys(jsonObject.getString("password"));
			getDriver().findElement(By.id("keepLoggedInCheckBox")).click();
			//getDriver().findElement(By.id("loginButton")).click();
			getDriver().findElement(By.tagName("a")).click();
			//getDriver().findElement(By.className("initial")).click();
			getDriver().findElement(By.xpath("//span[text()='Username or Password is invalid. Please try again.']"));
		}
	}
	
	private void checkEmptyValue() throws Exception{
		WebElement userNameWE=getDriver().findElement(By.name("username"));
		WebElement passwordWE=getDriver().findElement(By.name("password"));
		if(!userNameWE.getAttribute("value").isEmpty() || 
				!passwordWE.getAttribute("value").isEmpty()) {			
			System.out.println("Expected Username/pasword field empty");
			throw new Exception();
		}
		
	}

	private void testSecurity() throws Exception {
		JsonArray invalidLoginIdsArray = jsonObject.getJsonArray("insecureLoginIds");
		for (JsonValue jsonValue : invalidLoginIdsArray) {	
			JsonObject jsonObject = (JsonObject)jsonValue;
			clearLoginForm();
			checkEmptyValue();
			getDriver().findElement(By.name("username")).sendKeys(jsonObject.getString("username"));
			getDriver().findElement(By.name("pwd")).sendKeys(jsonObject.getString("password"));
			getDriver().findElement(By.id("keepLoggedInCheckBox")).click();
			//getDriver().findElement(By.id("loginButton")).click();
			getDriver().findElement(By.tagName("a")).click();
			//getDriver().findElement(By.className("initial")).click();
			getDriver().findElement(By.xpath("//span[text()='Username or Password is invalid. Please try again.']"));	
		}
	}
	
	private void testPositive() throws Exception {
		JsonArray validLoginIdsArray = jsonObject.getJsonArray("validLoginIds");
		JsonObject adminIdObj = validLoginIdsArray.getJsonObject(0);
		
		clearLoginForm();
		checkEmptyValue();
		
		getDriver().findElement(By.name("username")).sendKeys(adminIdObj.getString("username"));
		getDriver().findElement(By.name("pwd")).sendKeys(adminIdObj.getString("password"));
		getDriver().findElement(By.id("keepLoggedInCheckBox")).click();
		getDriver().findElement(By.id("loginButton")).click();
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.titleContains("actiTIME - Enter Time-Track"));
	}
	
	private void clearLoginForm() {
		getDriver().findElement(By.name("username")).clear();
		getDriver().findElement(By.name("pwd")).clear();
	}

	/**
	 * @return the jsonObject
	 */
	public JsonObject getJsonObject() {
		return jsonObject;
	}

	/**
	 * @param jsonObject the jsonObject to set
	 */
	public void setJsonObject(JsonObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	/**
	 * @return the driver
	 */
	public WebDriver getDriver() {
		return driver;
	}

	/**
	 * @param driver the driver to set
	 */
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	
}
