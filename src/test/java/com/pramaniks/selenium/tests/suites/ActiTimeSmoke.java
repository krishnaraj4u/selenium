package com.pramaniks.selenium.tests.suites;

import javax.json.JsonObject;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.pramaniks.selenium.DriverFactory;
import com.pramaniks.selenium.tests.pageObjects.DashboardPage;
import com.pramaniks.selenium.tests.pageObjects.LoginPage;
import com.pramaniks.selenium.utils.CommonUtil;

public class ActiTimeSmoke extends DriverFactory {

	@Test
    public void actiTimeLogin() throws Exception {
        WebDriver driver = getDriver();
        JsonObject jsonObject = CommonUtil.getJsonObject("loginData.json");
        
        getDriver().get("http://127.0.0.1/login.do");
        
        LoginPage loginPage = new LoginPage(driver,jsonObject);
        loginPage.testLoginPage();
        
        DashboardPage dashboardPage = new DashboardPage(driver,jsonObject);
        dashboardPage.testDashboardPage();
   }
}